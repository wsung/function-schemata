'use strict';

const path = require( 'path' );
const {
	convertFormattedRequestToVersionedBinary,
	convertWrappedZObjectToVersionedBinary,
	convertZObjectToBinary,
	getWrappedZObjectFromVersionedBinary,
	getZObjectFromBinary,
	formatNormalForSerialization,
	recoverNormalFromSerialization
} = require( '../../../src/serialize.js' );
const { readJSON } = require( '../../../src/fileUtils.js' );

QUnit.module( 'serialization' );

const hugeFunctionCall = readJSON( path.join( 'test_data', 'function_call', 'too_big.json' ) );

// Sense check: serialized version should be smaller than the original object.
QUnit.test( 'binaries should be small', ( assert ) => {
	const serialized = convertZObjectToBinary( hugeFunctionCall );
	const compressedSize = serialized.length;
	const uncompressedSize = JSON.stringify( hugeFunctionCall ).length;
	// For objects of this size, we should achieve more than 2.5x compression.
	assert.true( compressedSize * 2.5 < uncompressedSize );
} );

// Idempotence check: serialization then deserialization should produce the
// original object.
QUnit.test( 'serialization 0.0.1 should be invertible', ( assert ) => {
	const serialized = convertZObjectToBinary( hugeFunctionCall );
	const deserialized = getZObjectFromBinary( serialized );
	assert.deepEqual( deserialized, hugeFunctionCall );
} );

QUnit.test( 'recoverNormalFromSerialization should throw error on bad input', ( assert ) => {
	const degenerate = { 'ztypes.Z2': 'impossible' };
	assert.throws( () => {
		recoverNormalFromSerialization( degenerate );
	} );
} );

QUnit.test( 'serialization 0.0.2 should be invertible', ( assert ) => {
	const original = {
		reentrant: true,
		zobject: hugeFunctionCall
	};
	const serialized = convertZObjectToBinary( original, '0.0.2' );
	const deserialized = getZObjectFromBinary( serialized, '0.0.2' );
	assert.deepEqual( deserialized, original );
} );

QUnit.test( 'wrapped serialization should be invertible with version 0.0.1', ( assert ) => {
	const original = hugeFunctionCall;
	const serialized = convertWrappedZObjectToVersionedBinary( original, '0.0.1' );
	const deserialized = getWrappedZObjectFromVersionedBinary( serialized );
	assert.deepEqual( deserialized, original );
} );

QUnit.test( 'wrapped serialization should be invertible with version 0.0.2', ( assert ) => {
	const original = {
		reentrant: true,
		zobject: hugeFunctionCall
	};
	const serialized = convertWrappedZObjectToVersionedBinary( original, '0.0.2' );
	const deserialized = getWrappedZObjectFromVersionedBinary( serialized );
	assert.deepEqual( deserialized, original );
} );

QUnit.test( 'formatNormalForSerialization/recoverNormalFromSerialization: quote', ( assert ) => {
	// FIXME: Why does validatesAsQuote() require normal form but the others require canonical?
	const quote = { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z99' }, Z99K1: 'Hello, this is dog' };
	const expected = { 'ztypes.Z99': { Z99K1: '"Hello, this is dog"' } };
	const normalFormQuote = formatNormalForSerialization( quote );
	assert.deepEqual( normalFormQuote, expected );

	const recoveredForm = recoverNormalFromSerialization( normalFormQuote );
	assert.deepEqual( recoveredForm, quote );
} );

QUnit.test( 'formatNormalForSerialization/recoverNormalFromSerialization: reference', ( assert ) => {
	const reference = { Z1K1: 'Z9', Z9K1: 'Z1' };
	const expected = { 'ztypes.Z9': { Z9K1: 'Z1' } };
	const normalFormReference = formatNormalForSerialization( reference );
	assert.deepEqual( normalFormReference, expected );

	const recoveredForm = recoverNormalFromSerialization( normalFormReference );
	assert.deepEqual( recoveredForm, reference );
} );

QUnit.test( 'formatNormalForSerialization/recoverNormalFromSerialization: string', ( assert ) => {
	const string = { Z1K1: 'Z6', Z6K1: 'String value' };
	const expected = { 'ztypes.Z6': { Z6K1: 'String value' } };
	const normalFormString = formatNormalForSerialization( string );
	assert.deepEqual( normalFormString, expected );

	const recoveredForm = recoverNormalFromSerialization( normalFormString );
	assert.deepEqual( recoveredForm, string );
} );

QUnit.test( 'formatNormalForSerialization/recoverNormalFromSerialization: custom type', ( assert ) => {
	const custom = { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z400' }, Z400K1: { Z1K1: 'Z6', Z6K1: 'Inner string value' } };
	const expected = { 'ztypes.Z1': { valueMap: {
		Z1K1: { 'ztypes.Z9': { Z9K1: 'Z400' } },
		Z400K1: { 'ztypes.Z6': { Z6K1: 'Inner string value' } }
	} } };
	const normalFormCustom = formatNormalForSerialization( custom );
	assert.deepEqual( normalFormCustom, expected );

	const recoveredForm = recoverNormalFromSerialization( normalFormCustom );
	assert.deepEqual( recoveredForm, custom );
} );

QUnit.test( 'serialization 0.0.3', ( assert ) => {
	const bigFunctionCall = readJSON( path.join( 'test_data', 'function_call', 'decently_big.json' ) );
	const original = {
		reentrant: true,
		zobject: bigFunctionCall
	};
	const serialized = convertWrappedZObjectToVersionedBinary( original, '0.0.3' );
	const deserialized = getWrappedZObjectFromVersionedBinary( serialized );
	const expected = {
		reentrant: true,
		codingLanguage: 'python-3',
		codeString: 'def Z1802(Z1802K1, Z1802K2):\n    return {Z1802K2: str(Z1802K1[Z1802K2])}',
		functionName: 'Z1802',
		functionArguments: {
			Z1802K1: {
				Z1K1: 'Z6',
				Z6K1: 'true?'
			}
		}
	};

	assert.deepEqual( deserialized, expected );
} );

QUnit.test( 'serialization 0.0.4', ( assert ) => {
	const bigFunctionCall = readJSON( path.join( 'test_data', 'function_call', 'decently_big.json' ) );
	const e = 2.718281828459;
	const original = {
		reentrant: true,
		zobject: bigFunctionCall,
		remainingTime: e
	};
	const serialized = convertWrappedZObjectToVersionedBinary( original, '0.0.4' );
	const deserialized = getWrappedZObjectFromVersionedBinary( serialized );
	const expected = {
		reentrant: true,
		remainingTime: e,
		codingLanguage: 'python-3',
		codeString: 'def Z1802(Z1802K1, Z1802K2):\n    return {Z1802K2: str(Z1802K1[Z1802K2])}',
		functionName: 'Z1802',
		functionArguments: {
			Z1802K1: {
				Z1K1: 'Z6',
				Z6K1: 'true?'
			}
		}
	};

	assert.deepEqual( deserialized, expected );
} );

QUnit.test( 'serialization 0.0.5', ( assert ) => {
	const bigFunctionCall = readJSON( path.join( 'test_data', 'function_call', 'decently_big.json' ) );
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: bigFunctionCall,
		remainingTime: e,
		requestId: requestId
	};
	const serialized = convertWrappedZObjectToVersionedBinary( original, '0.0.5' );
	const deserialized = getWrappedZObjectFromVersionedBinary( serialized );
	const expected = {
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'python-3',
		codeString: 'def Z1802(Z1802K1, Z1802K2):\n    return {Z1802K2: str(Z1802K1[Z1802K2])}',
		functionName: 'Z1802',
		functionArguments: {
			Z1802K1: {
				Z1K1: 'Z6',
				Z6K1: 'true?'
			}
		}
	};

	assert.deepEqual( deserialized, expected );
} );

QUnit.test( 'serialization 0.1.0 without derializers', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'decently_big.json' ) );
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const serialized = convertWrappedZObjectToVersionedBinary( original, '0.1.0', true );
	const deserialized = getWrappedZObjectFromVersionedBinary( serialized );
	const expected = {
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'python-3',
		serializer: null,
		function: {
			codeString: 'def Z1802(Z1802K1, Z1802K2):\n    return {Z1802K2: str(Z1802K1[Z1802K2])}',
			functionName: 'Z1802'
		},
		functionArguments: {
			Z1802K1: {
				deserializer: null,
				object: {
					Z1K1: 'Z6',
					Z6K1: 'true?'
				}
			}
		}
	};

	assert.deepEqual( deserialized, expected );
} );

QUnit.test( 'serialization 0.1.0 with ZReference for programming language', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'decently_big.json' ) );
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	functionCall.Z7K1.Z8K4.K1.Z14K3.Z16K1 = { Z1K1: 'Z9', Z9K1: 'Z610' };
	const serialized = convertWrappedZObjectToVersionedBinary( original, '0.1.0', true );
	const deserialized = getWrappedZObjectFromVersionedBinary( serialized );
	const expected = {
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'Z610',
		serializer: null,
		function: {
			codeString: 'def Z1802(Z1802K1, Z1802K2):\n    return {Z1802K2: str(Z1802K1[Z1802K2])}',
			functionName: 'Z1802'
		},
		functionArguments: {
			Z1802K1: {
				deserializer: null,
				object: {
					Z1K1: 'Z6',
					Z6K1: 'true?'
				}
			}
		}
	};

	assert.deepEqual( deserialized, expected );
} );

QUnit.test( 'serialization 0.1.0 with derializers but enableDerializers is false', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_serialization.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const serialized = convertWrappedZObjectToVersionedBinary( original, '0.1.0', /* enableDerializers= */ false );
	const deserialized = getWrappedZObjectFromVersionedBinary( serialized );
	const expected = {
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		serializer: null,
		functionArguments: {
			Z400K1: {
				deserializer: null,
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				}
			}
		}
	};

	assert.deepEqual( deserialized, expected );
} );

QUnit.test( 'serialization 0.1.0 with derializers', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_serialization.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const serialized = convertWrappedZObjectToVersionedBinary( original, '0.1.0', /* enableDerializers= */ true );
	const deserialized = getWrappedZObjectFromVersionedBinary( serialized );
	const expected = {
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		serializer: {
			codeString: (
				'function Z10003( Z10001 ) { ' +
                'return { ' +
                'Z1K1: { Z1K1: \'Z9\', Z9K1: \'Z10001\' }, ' +
                'Z10001K1: { Z1K1: \'Z6\', Z6K1: Z10001.toString() } ' +
                '}; }' ),
			functionName: 'Z10003'
		},
		functionArguments: {
			Z400K1: {
				deserializer: {
					codeString: 'function Z10002( Z10001 ) { return parseInt( Z10001.Z10001K1.Z6K1 ); }',
					functionName: 'Z10002'
				},
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				}
			}
		}
	};

	assert.deepEqual( deserialized, expected );
} );

QUnit.test( 'serialization 0.1.0 with nontrivial (de)serializer IDs', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_serialization.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	const oldDeserializer = Z10001Type.Z4K7;
	const oldSerializer = Z10001Type.Z4K8;

	// Now the Identity fields are full Z46/Z64 instead of references.
	oldDeserializer.Z46K1 = { ...oldDeserializer };
	oldSerializer.Z64K1 = { ...oldSerializer };
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const serialized = convertWrappedZObjectToVersionedBinary( original, '0.1.0', /* enableDerializers= */ true );
	const deserialized = getWrappedZObjectFromVersionedBinary( serialized );
	const expected = {
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		serializer: {
			codeString: (
				'function Z10003( Z10001 ) { ' +
                'return { ' +
                'Z1K1: { Z1K1: \'Z9\', Z9K1: \'Z10001\' }, ' +
                'Z10001K1: { Z1K1: \'Z6\', Z6K1: Z10001.toString() } ' +
                '}; }' ),
			functionName: 'Z10003'
		},
		functionArguments: {
			Z400K1: {
				deserializer: {
					codeString: 'function Z10002( Z10001 ) { return parseInt( Z10001.Z10001K1.Z6K1 ); }',
					functionName: 'Z10002'
				},
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				}
			}
		}
	};

	assert.deepEqual( deserialized, expected );
} );

QUnit.test( 'serialization 0.1.0 with no programming language match', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_serialization.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );

	// Now no (de)serializer matches the desired programming language.
	Z10001Type.Z4K7.K1.Z46K3.Z16K1.Z61K1.Z6K1 = 'python-3';
	Z10001Type.Z4K8.K1.Z64K3.Z16K1.Z61K1.Z6K1 = 'python-3';
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	const serialized = convertWrappedZObjectToVersionedBinary( original, '0.1.0', /* enableDerializers= */ true );
	const deserialized = getWrappedZObjectFromVersionedBinary( serialized );
	const expected = {
		reentrant: true,
		remainingTime: e,
		requestId: requestId,
		codingLanguage: 'javascript',
		serializer: null,
		function: {
			codeString: 'function Z400( Z400K1 ) { return Z400K1 + 1; }',
			functionName: 'Z400'
		},
		functionArguments: {
			Z400K1: {
				object: {
					Z1K1: Z10001Type.Z4K1,
					Z10001K1: {
						Z1K1: 'Z6',
						Z6K1: '439'
					}
				},
				deserializer: null
			}
		}
	};

	assert.deepEqual( deserialized, expected );
} );

QUnit.test( 'serialization 0.1.0 with bad Function Identity', ( assert ) => {
	const functionCall = readJSON( path.join( 'test_data', 'function_call', 'with_serialization.json' ) );
	const Z10001Type = readJSON( path.join( 'test_data', 'function_call', 'Z10001-type.json' ) );
	functionCall.Z7K1.Z8K2 = Z10001Type;
	functionCall.Z400K1.Z1K1 = Z10001Type;
	functionCall.Z7K1.Z8K5 = { Z1K1: 'Z6', Z6K1: 'Z400' };
	const e = 2.718281828459;
	const requestId = 'e29a9fb0-0579-11ee-a8d3-f98479c72c01';
	const original = {
		reentrant: true,
		zobject: functionCall,
		remainingTime: e,
		requestId: requestId
	};
	assert.raises( () => {
		convertWrappedZObjectToVersionedBinary( original, '0.1.0' );
	} );
} );

QUnit.test( 'convert formatted request back to binary', ( assert ) => {
	const formattedRequest = readJSON( path.join( 'test_data', 'formatted_request', 'python3_add_with_derializers.json' ) );
	const serialized = convertFormattedRequestToVersionedBinary( formattedRequest, '0.1.0' );
	const actual = getWrappedZObjectFromVersionedBinary( serialized );
	assert.deepEqual( actual, formattedRequest );
} );

QUnit.test( 'convert formatted request back to binary, no derializers', ( assert ) => {
	const formattedRequest = readJSON( path.join( 'test_data', 'formatted_request', 'python3_add_with_derializers.json' ) );
	delete formattedRequest.serializer;
	for ( const key of Object.keys( formattedRequest.functionArguments ) ) {
		delete formattedRequest.functionArguments[ key ].deserializer;
	}
	const serialized = convertFormattedRequestToVersionedBinary( formattedRequest, '0.1.0' );
	const actual = getWrappedZObjectFromVersionedBinary( serialized );

	// Set de/serializers to "null."
	const expected = { ...formattedRequest };
	expected.serializer = null;
	for ( const key of Object.keys( expected.functionArguments ) ) {
		expected.functionArguments[ key ].deserializer = null;
	}
	assert.deepEqual( actual, expected );
} );

QUnit.test( 'convert formatted request back to binary, null derializers', ( assert ) => {
	const formattedRequest = readJSON( path.join( 'test_data', 'formatted_request', 'python3_add_with_derializers.json' ) );
	formattedRequest.serializer = null;
	for ( const key of Object.keys( formattedRequest.functionArguments ) ) {
		formattedRequest.functionArguments[ key ].deserializer = null;
	}
	const serialized = convertFormattedRequestToVersionedBinary( formattedRequest, '0.1.0' );
	const actual = getWrappedZObjectFromVersionedBinary( serialized );
	assert.deepEqual( actual, formattedRequest );
} );

QUnit.test( 'convert formatted request back to binary throws Error for semver < 0.1.0', ( assert ) => {
	const formattedRequest = readJSON( path.join( 'test_data', 'formatted_request', 'python3_add_with_derializers.json' ) );
	assert.raises( () => {
		convertWrappedZObjectToVersionedBinary( formattedRequest, '0.0.5' );
	} );
} );
