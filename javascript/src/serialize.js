'use strict';

const avro = require( 'avro-js' );
const semver = require( 'semver' );
const { dataDir, readJSON } = require( './fileUtils.js' );
const { validatesAsQuote, validatesAsReference, validatesAsString } = require( './schema.js' );
const {
	convertZListToItemArray,
	findDeserializerIdentity,
	findFunctionIdentity,
	findIdentity,
	findSerializerIdentity,
	isZReference,
	isZType
} = require( './utils.js' );

const Z1_SCHEMA_ = readJSON( dataDir( 'avro', 'Z1.avsc' ) );
const Z6_SCHEMA_ = readJSON( dataDir( 'avro', 'Z6.avsc' ) );
const Z9_SCHEMA_ = readJSON( dataDir( 'avro', 'Z9.avsc' ) );
const Z99_SCHEMA_ = readJSON( dataDir( 'avro', 'Z99.avsc' ) );

// A ZObject is the union of arbitrary Z1s, Z6s, and Z9s.
// Note that Z1_SCHEMA_ must go last because it references the others by name.
const Z1_UNION_ = [ Z6_SCHEMA_, Z9_SCHEMA_, Z99_SCHEMA_, Z1_SCHEMA_ ];
// semver 0.0.1
const avroSchema = avro.parse( Z1_UNION_ );

// Requests in v0.0.2 also indicate whether to use reentrance.
const V002_REQUEST_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'ZOBJECT',
	fields: [
		{
			name: 'reentrant',
			type: 'boolean'
		},
		{
			name: 'zobject',
			type: Z1_UNION_
		}
	]
};
// semver 0.0.2
const zobjectSchemaV002 = avro.parse( V002_REQUEST_SCHEMA_ );

// Requests in v0.0.3 rely on a minimal set of information instead of an entire
// Z7, allowing for further compression and facilitating later extension.
const V003_REQUEST_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'ZOBJECT',
	fields: [
		{
			name: 'reentrant',
			type: 'boolean'
		},
		{
			name: 'codingLanguage',
			type: 'string'
		},
		{
			name: 'codeString',
			type: 'string'
		},
		{
			name: 'functionName',
			type: 'string'
		},
		{
			name: 'functionArguments',
			type: {
				type: 'map',
				values: Z1_UNION_
			}
		}
	]
};
// semver 0.0.3
const requestSchemaV003 = avro.parse( V003_REQUEST_SCHEMA_ );

// Requests in v0.0.4 rely on a minimal set of information instead of an entire
// Z7, allowing for further compression and facilitating later extension.
const V004_REQUEST_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'ZOBJECT',
	fields: [
		{
			name: 'reentrant',
			type: 'boolean'
		},
		{
			name: 'remainingTime',
			type: 'double'
		},
		{
			name: 'codingLanguage',
			type: 'string'
		},
		{
			name: 'codeString',
			type: 'string'
		},
		{
			name: 'functionName',
			type: 'string'
		},
		{
			name: 'functionArguments',
			type: {
				type: 'map',
				values: Z1_UNION_
			}
		}
	]
};
// semver 0.0.4
const requestSchemaV004 = avro.parse( V004_REQUEST_SCHEMA_ );

// v0.0.5: like v0.0.4 but with a request ID!
const V005_REQUEST_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'ZOBJECT',
	fields: [
		{
			name: 'reentrant',
			type: 'boolean'
		},
		{
			name: 'remainingTime',
			type: 'double'
		},
		{
			name: 'codingLanguage',
			type: 'string'
		},
		{
			name: 'codeString',
			type: 'string'
		},
		{
			name: 'functionName',
			type: 'string'
		},
		{
			name: 'requestId',
			type: 'string'
		},
		{
			name: 'functionArguments',
			type: {
				type: 'map',
				values: Z1_UNION_
			}
		}
	]
};
// semver 0.0.5
const requestSchemaV005 = avro.parse( V005_REQUEST_SCHEMA_ );

// v0.1.0: first new version after release.
// Like v0.0.5 but with serializers and deserializers.
// Also encapsulates functionName, codeString in a CODE record.
const CODE_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'CODE',
	fields: [
		{
			name: 'codeString',
			type: 'string'
		},
		{
			name: 'functionName',
			type: 'string'
		}
	]
};
const ARGUMENT_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'ARGUMENT',
	fields: [
		{
			name: 'deserializer',
			type: [ 'null', 'ztypes.CODE' ],
			default: null
		},
		{
			name: 'object',
			type: Z1_UNION_
		}
	]
};
const V010_REQUEST_SCHEMA_ = {
	type: 'record',
	namespace: 'ztypes',
	name: 'ZOBJECT',
	fields: [
		{
			name: 'reentrant',
			type: 'boolean'
		},
		{
			name: 'remainingTime',
			type: 'double'
		},
		{
			name: 'requestId',
			type: 'string'
		},
		{
			name: 'codingLanguage',
			type: 'string'
		},
		{
			name: 'function',
			type: CODE_SCHEMA_
		},
		{
			name: 'serializer',
			type: [ 'null', 'ztypes.CODE' ],
			default: null
		},
		{
			name: 'functionArguments',
			type: {
				type: 'map',
				values: ARGUMENT_SCHEMA_
			}
		}
	]
};
// semver 0.1.0
const requestSchemaV010 = avro.parse( V010_REQUEST_SCHEMA_ );

/**
 * Transform a ZObject into the form expected by the serialization schema.
 *
 * @param {Object} ZObject a normal-form ZObject
 * @return {Object} an object in the intermediate serialization format
 */
function formatNormalForSerialization( ZObject ) {
	if ( validatesAsQuote( ZObject ).isValid() ) {
		return { 'ztypes.Z99': { Z99K1: JSON.stringify( ZObject.Z99K1 ) } };
	}
	if ( validatesAsReference( ZObject ).isValid() ) {
		return { 'ztypes.Z9': { Z9K1: ZObject.Z9K1 } };
	}
	if ( validatesAsString( ZObject ).isValid() ) {
		return { 'ztypes.Z6': { Z6K1: ZObject.Z6K1 } };
	}
	const valueMap = {};
	for ( const key of Object.keys( ZObject ) ) {
		valueMap[ key ] = formatNormalForSerialization( ZObject[ key ] );
	}
	return { 'ztypes.Z1': { valueMap: valueMap } };
}

/**
 * Recover a ZObject from the form imposed by the serialization schema.
 *
 * @param {Object} serializedZObject a ZObject in the intermediate serialization format
 * @return {Object} a normal-form ZObject
 */
function recoverNormalFromSerialization( serializedZObject ) {
	const Z1 = serializedZObject[ 'ztypes.Z1' ];
	if ( Z1 !== undefined ) {
		const result = {};
		for ( const key of Object.keys( Z1.valueMap ) ) {
			result[ key ] = recoverNormalFromSerialization( Z1.valueMap[ key ] );
		}
		return result;
	}
	const Z6 = serializedZObject[ 'ztypes.Z6' ];
	if ( Z6 !== undefined ) {
		return { Z1K1: 'Z6', Z6K1: Z6.Z6K1 };
	}
	const Z9 = serializedZObject[ 'ztypes.Z9' ];
	if ( Z9 !== undefined ) {
		return { Z1K1: 'Z9', Z9K1: Z9.Z9K1 };
	}
	const Z99 = serializedZObject[ 'ztypes.Z99' ];
	if ( Z99 !== undefined ) {
		return {
			Z1K1: {
				Z1K1: 'Z9',
				Z9K1: 'Z99'
			},
			Z99K1: JSON.parse( Z99.Z99K1 )
		};
	}
	throw new Error( 'Invalid serialization form; must define one of ztypes.(Z1|Z6|Z9|Z99}' );
}

function programmingLanguagesMatch( desiredLanguage, retrievedLanguage ) {
	for ( const language of [ 'python', 'javascript' ] ) {
		if (
			desiredLanguage.startsWith( language ) &&
                retrievedLanguage.startsWith( language ) ) {
			return true;
		}
	}
	return false;
}

function findDerializerForProgrammingLanguage( desiredLanguage, derializerList, getCode ) {
	const derializerArray = (
		( derializerList === undefined ) ?
			[] : convertZListToItemArray( derializerList ) );
	for ( const derializer of derializerArray ) {
		const retrievedLanguage = getCode( derializer ).Z16K1.Z61K1.Z6K1;
		if ( programmingLanguagesMatch( desiredLanguage, retrievedLanguage ) ) {
			return derializer;
		}
	}
	return null;
}

/**
 * Given a programming language and a list of serializers or deserializers, find
 * a serializer or deserializer whose programming language is compatible with the
 * desired language.
 *
 * @param {string} desiredLanguage the desired programming lanugage
 * @param {Object} derializerList a Z881/List of either Z46/Deserializer or Z64/Serializer
 * @param {Function} getCode callback which retrieves the Code object from a given Z46 or Z64
 * @param {Function} getIdentity get Z9/Reference Identity for a Z46 or Z64
 * @return {Object|null} a matching (de) serializer, or null if none is found
 */
function getCodeObjectForDerializerList(
	desiredLanguage, derializerList, getCode, getIdentity ) {
	const derializer = findDerializerForProgrammingLanguage(
		desiredLanguage,
		derializerList,
		getCode );
	if ( derializer === null ) {
		return null;
	}
	// The (De)Serializer's Identity must be a Z9/Reference or we can't proceed.
	const derializerIdentity = getIdentity( derializer );
	if ( derializerIdentity === null ) {
		return null;
	}
	const derializerCode = getCode( derializer );
	return {
		'ztypes.CODE': {
			codeString: derializerCode.Z16K2.Z6K1,
			functionName: derializerIdentity.Z9K1
		}
	};
}

function convertRequestToBinaryV010Plus( functionCallRequest, enableDerializers = false ) {
	const toSerialize = {
		reentrant: functionCallRequest.reentrant,
		remainingTime: functionCallRequest.remainingTime,
		requestId: functionCallRequest.requestId,
		function: {},
		functionArguments: {}
	};

	const functionCall = functionCallRequest.zobject;
	const theFunction = functionCall.Z7K1;

	// We assume that the orchestrator will have already selected a single implementation,
	// so the first implementation will be the only one.
	const firstImplementation = theFunction.Z8K4.K1;
	const functionCode = firstImplementation.Z14K3;

	const programmingLanguageObject = functionCode.Z16K1;
	let programmingLanguage;
	if ( isZReference( programmingLanguageObject ) ) {
		programmingLanguage = programmingLanguageObject.Z9K1;
	} else {
		programmingLanguage = programmingLanguageObject.Z61K1.Z6K1;
	}
	toSerialize.codingLanguage = programmingLanguage;
	toSerialize.function.codeString = functionCode.Z16K2.Z6K1;
	const functionIdentity = findFunctionIdentity( theFunction );
	if ( functionIdentity === null ) {
		throw new Error( 'Identity of Z8 was not a Z9: ' + JSON.stringify( theFunction ) );
	}
	toSerialize.function.functionName = functionIdentity.Z9K1;

	// Add a serializer if the information is present in the function's return type.
	// If return type is not given as a Z4/Type, or if it lacks the Z4K8 key,
	// we cannot populate the serializer.
	const returnType = theFunction.Z8K2;
	let serializerCode = null;
	if ( enableDerializers ) {
		serializerCode = getCodeObjectForDerializerList(
			programmingLanguage,
			returnType.Z4K8,
			( Z64 ) => Z64.Z64K3,
			findSerializerIdentity );
	}
	if ( serializerCode !== null ) {
		toSerialize.serializer = serializerCode;
	}

	for ( const key of Object.keys( functionCall ) ) {
		if ( key === 'Z1K1' || key === 'Z7K1' ) {
			continue;
		}
		const value = functionCall[ key ];
		const argument = {};
		let deserializerCode = null;
		if ( enableDerializers ) {
			deserializerCode = getCodeObjectForDerializerList(
				programmingLanguage,
				value.Z1K1.Z4K7,
				( Z46 ) => Z46.Z46K3,
				findDeserializerIdentity );
		}
		if ( deserializerCode !== null ) {
			argument.deserializer = deserializerCode;
		}

		// No reason to serialize the whole dang type.
		let valueToSerialize = value;
		if ( isZType( value.Z1K1 ) ) {
			const typeIdentity = findIdentity( value.Z1K1.Z4K1 );
			if ( typeIdentity !== null ) {
				valueToSerialize = { ...value };
				valueToSerialize.Z1K1 = typeIdentity;
			}
		}
		argument.object = formatNormalForSerialization( valueToSerialize );
		toSerialize.functionArguments[ key ] = argument;
	}
	return toSerialize;
}

function convertRequestToBinaryV003Plus( ZObject ) {
	const toSerialize = {
		reentrant: ZObject.reentrant,
		functionArguments: {}
	};

	// This is the only difference between semver v0.0.3 and v0.0.4.
	if ( ZObject.remainingTime !== null ) {
		toSerialize.remainingTime = ZObject.remainingTime;
	}
	// This is the only difference between semver v0.0.4 and v0.0.5.
	if ( ZObject.requestId !== null ) {
		toSerialize.requestId = ZObject.requestId;
	}

	const actualObject = ZObject.zobject;
	for ( const key of Object.keys( actualObject ) ) {
		if ( key === 'Z1K1' ) {
			continue;
		}
		const value = actualObject[ key ];
		if ( key === 'Z7K1' ) {
			toSerialize.functionName = value.Z8K5.Z9K1;
			const firstImplementation = value.Z8K4.K1;
			toSerialize.codingLanguage = firstImplementation.Z14K3.Z16K1.Z61K1.Z6K1;
			toSerialize.codeString = firstImplementation.Z14K3.Z16K2.Z6K1;
			continue;
		}
		toSerialize.functionArguments[ key ] = formatNormalForSerialization( value );
	}
	return toSerialize;
}

function convertFormattedRequestToBinaryV010Plus( request ) {
	const result = { ...request };
	result.functionArguments = {};
	for ( const key of Object.keys( request.functionArguments ) ) {
		const argument = {};
		result.functionArguments[ key ] = argument;
		const original = request.functionArguments[ key ];
		argument.object = formatNormalForSerialization( original.object );
		if ( original.deserializer ) {
			argument.deserializer = { 'ztypes.CODE': original.deserializer };
		}
	}
	if ( result.serializer ) {
		result.serializer = { 'ztypes.CODE': result.serializer };
	} else {
		delete result.serializer;
	}
	return result;
}

function convertFormattedRequestToBinary( request, version ) {
	if ( semver.gte( version, '0.1.0' ) ) {
		const toSerialize = convertFormattedRequestToBinaryV010Plus( request );
		return requestSchemaV010.toBuffer( toSerialize );
	}
	throw new Error( 'Formatted request -> binary operation only supported for semvers >= 0.1.0' );
}

function convertZObjectToBinary( ZObject, version = '0.0.1', enableDerializers = false ) {
	if ( semver.gte( version, '0.1.0' ) ) {
		const toSerialize = convertRequestToBinaryV010Plus( ZObject, enableDerializers );
		return requestSchemaV010.toBuffer( toSerialize );
	}
	if ( semver.gte( version, '0.0.3' ) ) {
		const toSerialize = convertRequestToBinaryV003Plus( ZObject );
		if ( semver.gte( version, '0.0.5' ) ) {
			return requestSchemaV005.toBuffer( toSerialize );
		}
		if ( semver.gte( version, '0.0.4' ) ) {
			return requestSchemaV004.toBuffer( toSerialize );
		}
		return requestSchemaV003.toBuffer( toSerialize );
	}
	if ( semver.gte( version, '0.0.2' ) ) {
		return zobjectSchemaV002.toBuffer( {
			reentrant: ZObject.reentrant,
			zobject: formatNormalForSerialization( ZObject.zobject )
		} );
	}
	return avroSchema.toBuffer( formatNormalForSerialization( ZObject ) );
}

function recoverRequestFromAvroFormat010Plus( recovered ) {
	// Copy all members so that the result loses the ZOBJECT type annotation.
	const result = {
		reentrant: recovered.reentrant,
		remainingTime: recovered.remainingTime,
		requestId: recovered.requestId,
		codingLanguage: recovered.codingLanguage,
		function: {},
		functionArguments: {}
	};
	result.function.codeString = recovered.function.codeString;
	result.function.functionName = recovered.function.functionName;
	if ( recovered.serializer === null ) {
		result.serializer = null;
	} else {
		const rawSerializer = recovered.serializer[ 'ztypes.CODE' ];
		const serializer = {};
		serializer.codeString = rawSerializer.codeString;
		serializer.functionName = rawSerializer.functionName;
		result.serializer = serializer;
	}
	result.functionArguments = {};
	for ( const key of Object.keys( recovered.functionArguments ) ) {
		const argument = {};
		const original = recovered.functionArguments[ key ];
		argument.object = recoverNormalFromSerialization( original.object );
		if ( original.deserializer === null ) {
			argument.deserializer = null;
		} else {
			const rawDeserializer = original.deserializer[ 'ztypes.CODE' ];
			const deserializer = {};
			deserializer.codeString = rawDeserializer.codeString;
			deserializer.functionName = rawDeserializer.functionName;
			argument.deserializer = deserializer;
		}
		result.functionArguments[ key ] = argument;
	}
	return result;
}

function recoverRequestFromAvroFormat3Plus( recovered ) {
	// Copy all members so that the result loses the ZOBJECT type annotation.
	const result = { ...recovered };
	for ( const key of Object.keys( recovered.functionArguments ) ) {
		const argument = recovered.functionArguments[ key ];
		result.functionArguments[ key ] = recoverNormalFromSerialization( argument );
	}
	return result;
}

function getZObjectFromBinary( buffer, version = '0.0.1' ) {
	if ( semver.gte( version, '0.1.0' ) ) {
		const recovered = requestSchemaV010.fromBuffer( buffer );
		return recoverRequestFromAvroFormat010Plus( recovered );
	}
	if ( semver.gte( version, '0.0.3' ) ) {
		let recovered;
		if ( semver.gte( version, '0.0.5' ) ) {
			recovered = requestSchemaV005.fromBuffer( buffer );
		} else if ( semver.gte( version, '0.0.4' ) ) {
			recovered = requestSchemaV004.fromBuffer( buffer );
		} else {
			recovered = requestSchemaV003.fromBuffer( buffer );
		}
		return recoverRequestFromAvroFormat3Plus( recovered );
	}
	if ( semver.gte( version, '0.0.2' ) ) {
		const recovered = zobjectSchemaV002.fromBuffer( buffer );
		return {
			reentrant: recovered.reentrant,
			zobject: recoverNormalFromSerialization( recovered.zobject )
		};
	}
	return recoverNormalFromSerialization( avroSchema.fromBuffer( buffer ) );
}

function addVersionToBuffer( serialized, version ) {
	const semverBuffer = Buffer.from( version );
	const lengthBuffer = Buffer.from( String.fromCharCode( semverBuffer.length ) );
	return Buffer.concat( [ lengthBuffer, semverBuffer, serialized ] );
}

function convertFormattedRequestToVersionedBinary( request, version ) {
	const serialized = convertFormattedRequestToBinary( request, version );
	return addVersionToBuffer( serialized, version );
}

function convertWrappedZObjectToVersionedBinary(
	wrappedZObject, version, enableDerializers = false ) {
	const serialized = convertZObjectToBinary( wrappedZObject, version, enableDerializers );
	return addVersionToBuffer( serialized, version );
}

function getWrappedZObjectFromVersionedBinary( buffer ) {
	const length = buffer.slice( 0, 1 );
	const lengthPivot = 1 + length.toString().charCodeAt( 0 );
	const version = buffer.slice( 1, lengthPivot ).toString();
	const avroSerialized = buffer.slice( lengthPivot, buffer.length );
	return getZObjectFromBinary( avroSerialized, version );
}

module.exports = {
	avroSchema,
	convertFormattedRequestToVersionedBinary,
	convertWrappedZObjectToVersionedBinary,
	convertZObjectToBinary,
	getWrappedZObjectFromVersionedBinary,
	getZObjectFromBinary,
	formatNormalForSerialization,
	recoverNormalFromSerialization,
	zobjectSchemaV002
};
