'use strict';

const fs = require( 'fs' );
const path = require( 'path' );

QUnit.module( 'rollup' );

{
	const temporaryPath = 'temporary-utils.js';
	const utilsPath = path.join( 'javascript', 'src', 'utils.js' );
	const temporary = fs.readFileSync( temporaryPath, { encoding: 'utf8' } );
	const utils = fs.readFileSync( utilsPath, { encoding: 'utf8' } );

	QUnit.test( 'rollup test', ( assert ) => {
		assert.deepEqual( utils, temporary, 'Output of rollup does not match utils.js; please run `npm run rollup`.' );
	} );
}
